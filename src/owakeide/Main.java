package owakeide;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import owakeide.windows.OwakeideWindow;

public class Main {

	public static final Logger logger = LogManager.getLogger("Owakeide Mod Manager");

	public static boolean debug = true;

	public static final String type = "alpha";
	public static final String[] number = { "0", "1", "0" };
	public static final String release = "prerelease";
	public static final String version = type + "-" + number[0] + "." + number[1] + "." + number[2] + "-" + release;

	public static void main(String[] args) {

		Main.logger.info("Version: " + version);

		if (debug == true)
			Main.logger.info("Loading Main");

		try {
			OwakeideWindow.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
