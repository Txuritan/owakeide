package owakeide.tasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.eclipse.swt.SWT;

import owakeide.Main;
import owakeide.dialogs.InfoDialog;
import owakeide.windows.OwakeideWindow;

public class FileInfoDialogTask {
	public static final String file = "info.txt";
	
	public static void load() {
		Main.logger.info("Looking for info.txt");
		
		File f = new File(file);
		
		if(f.exists() && !f.isDirectory()) { 
			Main.logger.info("info.txt Found");
			Main.logger.info("Not Loading Info Dialog");
			Main.logger.info("If you wish to see this dialog delete: " + file);
		} else {
			Main.logger.info("info.txt Not Found");
			
			Main.logger.info("Creating info.txt");
			
			PrintWriter writer = null;
			
			try {
				writer = new PrintWriter(file, "UTF-8");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			writer.println("Do not delete this file");
			writer.println("This is ussed to control the inital info dialog");
			writer.close();
			
			Main.logger.info("Created info.txt");
			
			Main.logger.info("Loading Info Dialog");
			
			try {
				InfoDialog window = new InfoDialog(OwakeideWindow.shlOwakeide.getShell(), SWT.DIALOG_TRIM);
				window.open();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			Main.logger.info("Loaded Info Dialog");
		}
	}
}
