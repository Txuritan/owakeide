package owakeide.settings;

import java.io.File;

import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;

public class Settings {
	Parameters params = new Parameters();
	// Read data from this file
	File propertiesFile = new File("config.properties");
	
	public void openConfig() {
		@SuppressWarnings("unused")
		FileBasedConfigurationBuilder<FileBasedConfiguration> builder = new FileBasedConfigurationBuilder<FileBasedConfiguration>(
				PropertiesConfiguration.class).configure(params.fileBased().setFile(propertiesFile));
		//builder.
	}
}
