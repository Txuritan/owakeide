package owakeide.dialogs;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.SWT;

public class CheckForUpdatesDialog extends Dialog {

	protected Object result;
	protected Shell shlCheckForUpdates;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public CheckForUpdatesDialog(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shlCheckForUpdates.open();
		shlCheckForUpdates.layout();
		Display display = getParent().getDisplay();
		while (!shlCheckForUpdates.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlCheckForUpdates = new Shell(getParent(), getStyle());
		shlCheckForUpdates.setSize(450, 300);
		shlCheckForUpdates.setText("Check For Updates");
		shlCheckForUpdates.setLayout(new FillLayout(SWT.HORIZONTAL));

	}

}
