package owakeide.dialogs;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import owakeide.Main;

import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;

public class InfoDialog extends Dialog {

	protected Object result;
	protected Shell shlInfo;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public InfoDialog(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shlInfo.open();
		shlInfo.layout();
		Display display = getParent().getDisplay();
		while (!shlInfo.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	@SuppressWarnings("unused")
	private void createContents() {
		shlInfo = new Shell(getParent(), getStyle());
		shlInfo.setSize(450, 300);
		shlInfo.setText("Info");
		shlInfo.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		
		Label lblThis = new Label(shlInfo, SWT.WRAP | SWT.CENTER);
		if (Main.release == "prerelease") {
			lblThis.setText("Owakeide Mod Manager\r\n" + 
				"Version: " + Main.version + "\r\n\r\n" +
				"This programs release status is " + Main.release + ".\r\n\r\n" + 
				"It may not run correctly.\r\n\r\n" +
				"I recommend not resizing the program, Java Swing and SWT doesn't resize correcctly yet.\r\n\r\n" +
				"You will only see this once, unless you delete info.txt.");
		} else {
			lblThis.setText("Owakeide Mod Manager\r\n" + 
				"Version: " + Main.version + "\r\n\r\n" +
				"This programs release status is " + Main.release + ".\r\n\" + \r\n" + 
				"I recommend not resizing the program, Java Swing and SWT doesn't resize correcctly yet.\r\n\r\n" +
				"You will only see this once, unless you delete info.txt.");
		}
	}

}
