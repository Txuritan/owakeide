package owakeide.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class SettingsDialog extends Dialog {

	protected Object result;
	protected Shell shlSettings;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public SettingsDialog(Shell parent, int style) {
		super(parent, style);
		setText("Settings");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shlSettings.open();
		shlSettings.layout();
		Display display = getParent().getDisplay();
		while (!shlSettings.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlSettings = new Shell(getParent(), SWT.DIALOG_TRIM);
		shlSettings.setSize(450, 300);
		shlSettings.setText("Settings");
		shlSettings.setLayout(new FillLayout(SWT.VERTICAL));
		
		Composite composite = new Composite(shlSettings, SWT.NONE);
		composite.setLayout(new FillLayout(SWT.VERTICAL));
		
		SashForm sashForm = new SashForm(composite, SWT.VERTICAL);
		
		Composite composite_1 = new Composite(sashForm, SWT.NONE);
		composite_1.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		Button btnOkay = new Button(composite_1, SWT.NONE);
		btnOkay.setText("Okay");
		sashForm.setWeights(new int[] {1});

	}
}
