package owakeide.dialogs;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.jface.text.TextViewer;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import owakeide.Licenses;
import owakeide.Main;

import org.eclipse.swt.widgets.Label;

public class AboutDialog extends Dialog {

	protected Object result;
	protected Shell shlAbout;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public AboutDialog(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shlAbout.open();
		shlAbout.layout();
		Display display = getParent().getDisplay();
		while (!shlAbout.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlAbout = new Shell(getParent(), getStyle());
		shlAbout.setSize(450, 300);
		shlAbout.setText("About");
		shlAbout.setLayout(new FillLayout(SWT.VERTICAL));
		
		TabFolder tabFolder = new TabFolder(shlAbout, SWT.NONE);
		
		TabItem tbtmAbout = new TabItem(tabFolder, SWT.NONE);
		tbtmAbout.setText("About");
		
		Composite compositeAbout = new Composite(tabFolder, SWT.NONE);
		tbtmAbout.setControl(compositeAbout);
		compositeAbout.setLayout(new FillLayout(SWT.VERTICAL));
		
		Composite compositeAboutTop = new Composite(compositeAbout, SWT.NONE);
		compositeAboutTop.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		Label lblOwakeideModManager = new Label(compositeAboutTop, SWT.NONE);
		lblOwakeideModManager.setText("Owakeide Mod Manager\r\nVersion: " + Main.version);
		
		Composite compositeAboutBottom = new Composite(compositeAbout, SWT.NONE);
		compositeAboutBottom.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		Label lblCopyrightIan = new Label(compositeAboutBottom, SWT.NONE);
		lblCopyrightIan.setText("Copyright 2016 Ian Cronkright and contributors");
		
		TabItem tbtmLicenses = new TabItem(tabFolder, SWT.NONE);
		tbtmLicenses.setText("Licenses");
		
		Composite compositeLicenses = new Composite(tabFolder, SWT.NONE);
		tbtmLicenses.setControl(compositeLicenses);
		compositeLicenses.setLayout(new FillLayout(SWT.VERTICAL));
		
		Composite compositeTop = new Composite(compositeLicenses, SWT.NONE);
		compositeTop.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		ScrolledComposite scrolledComposite = new ScrolledComposite(compositeTop, SWT.BORDER | SWT.V_SCROLL);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
		
		final List list = new List(scrolledComposite, SWT.BORDER);
		list.setItems(new String[] {"Owakeide Mod Manager", "Eclipse SWT", "Apache Commons Configuration", "Apache Log4j", "Jackson"});
		scrolledComposite.setContent(list);
		scrolledComposite.setMinSize(list.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		Composite compositeBottom = new Composite(compositeLicenses, SWT.NONE);
		compositeBottom.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		ScrolledComposite scrolledComposite_1 = new ScrolledComposite(compositeBottom, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite_1.setExpandVertical(true);
		scrolledComposite_1.setExpandHorizontal(true);
		
		final TextViewer textViewer = new TextViewer(scrolledComposite_1, SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL);
		textViewer.setEditable(false);
		final StyledText styledText = textViewer.getTextWidget();
		
		list.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				int[] selection = list.getSelectionIndices();
				for (int i = 0; i < selection.length; i++) {
					if (selection[i] == 0) {
						styledText.setText(Licenses.licenseOwakeide);
					} else if (selection[i] == 1) {
						styledText.setText(Licenses.licenseEclipseSWT);
					} else if (selection[i] == 2 | selection[i] == 3 | selection[i] == 4) {
						styledText.setText(Licenses.licenseApacheCommonsConfiguration);
					}
			    }
			}
		});
		
		styledText.setEditable(false);
		scrolledComposite_1.setContent(styledText);
		scrolledComposite_1.setMinSize(styledText.computeSize(SWT.DEFAULT, SWT.DEFAULT));

	}
}
