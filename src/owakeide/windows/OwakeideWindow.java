package owakeide.windows;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

import owakeide.dialogs.AboutDialog;
import owakeide.dialogs.CheckForUpdatesDialog;
import owakeide.dialogs.SettingsDialog;
import owakeide.tasks.FileInfoDialogTask;

import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.jface.text.TextViewer;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;

public class OwakeideWindow {
	private static Table tableInstance;
	private static Table tableArchives;
	private static Table tableDownloads;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			OwakeideWindow.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public final static Shell shlOwakeide = new Shell();

	/**
	 * Open the window.
	 */
	public static void open() {
		Display display = Display.getDefault();
		shlOwakeide.setSize(707, 384);
		shlOwakeide.setText("Owakeide Mod Manager");
		shlOwakeide.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		Menu menu = new Menu(shlOwakeide, SWT.BAR);
		shlOwakeide.setMenuBar(menu);
		
		MenuItem mntmFile = new MenuItem(menu, SWT.CASCADE);
		mntmFile.setText("File");
		
		Menu menu_1 = new Menu(mntmFile);
		mntmFile.setMenu(menu_1);
		
		MenuItem mntmExit = new MenuItem(menu_1, SWT.NONE);
		mntmExit.setText("Exit");
		mntmExit.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				System.exit(0);
			}
		});
		
		MenuItem mntmSettings = new MenuItem(menu, SWT.NONE);
		mntmSettings.setText("Settings");
		mntmSettings.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				SettingsDialog dialog = new SettingsDialog(shlOwakeide.getShell(), SWT.DIALOG_TRIM);
				dialog.open();
			}
		});
		
		MenuItem mntmHelp = new MenuItem(menu, SWT.CASCADE);
		mntmHelp.setText("Help");
		
		Menu menu_2 = new Menu(mntmHelp);
		mntmHelp.setMenu(menu_2);
		
		MenuItem mntmCheckForUpdates = new MenuItem(menu_2, SWT.NONE);
		mntmCheckForUpdates.setText("Check For Updates");
		mntmCheckForUpdates.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				CheckForUpdatesDialog dialog = new CheckForUpdatesDialog(shlOwakeide.getShell(), SWT.DIALOG_TRIM);
				dialog.open();
			}
		});
		
		new MenuItem(menu_2, SWT.SEPARATOR);
		
		MenuItem mntmAboutOwakeide = new MenuItem(menu_2, SWT.NONE);
		mntmAboutOwakeide.setText("About Owakeide");
		
		SashForm sashForm = new SashForm(shlOwakeide, SWT.VERTICAL);
		
		Composite compositeTop = new Composite(sashForm, SWT.NONE);
		compositeTop.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		SashForm sashFormTop = new SashForm(compositeTop, SWT.NONE);
		
		Composite compositeLeft = new Composite(sashFormTop, SWT.BORDER);
		compositeLeft.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		SashForm sashFormTopLeft = new SashForm(compositeLeft, SWT.VERTICAL);
		
		Composite compositeLeftTop = new Composite(sashFormTopLeft, SWT.BORDER);
		compositeLeftTop.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		Label lblInstance = new Label(compositeLeftTop, SWT.CENTER);
		lblInstance.setText("Instance");
		
		CCombo comboInstance = new CCombo(compositeLeftTop, SWT.BORDER);
		comboInstance.setItems(new String[] {"<New Instance>", "Default (Newest)"});
		
		CCombo comboInstanceSettings = new CCombo(compositeLeftTop, SWT.BORDER);
		comboInstanceSettings.setItems(new String[] {"Create Backup", "Restore from Backup", "Export to JSON", "Export to CSV"});
		
		Composite compositeLeftBottom = new Composite(sashFormTopLeft, SWT.BORDER);
		compositeLeftBottom.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		tableInstance = new Table(compositeLeftBottom, SWT.BORDER | SWT.FULL_SELECTION);
		tableInstance.setHeaderVisible(true);
		tableInstance.setLinesVisible(true);
		sashFormTopLeft.setWeights(new int[] {1, 10});
		
		Composite compositeRight = new Composite(sashFormTop, SWT.BORDER);
		compositeRight.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		SashForm sashFormTopRight = new SashForm(compositeRight, SWT.VERTICAL);
		
		Composite compositeRightTop = new Composite(sashFormTopRight, SWT.BORDER);
		compositeRightTop.setLayout(new FillLayout(SWT.VERTICAL));
		
		Composite compositeRightTopTop = new Composite(compositeRightTop, SWT.BORDER);
		compositeRightTopTop.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		CCombo comboPlayInstanceSettings = new CCombo(compositeRightTopTop, SWT.BORDER);
		comboPlayInstanceSettings.setItems(new String[] {"Vanilla", "Forge", "Liteloader", "Forge + Liteloader", "Blazeloader"});
		
		Button btnPlay = new Button(compositeRightTopTop, SWT.NONE);
		btnPlay.setText("Play");
		
		Composite compositeRightTopBottom = new Composite(compositeRightTop, SWT.BORDER);
		compositeRightTopBottom.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		Label lblDownloads = new Label(compositeRightTopBottom, SWT.CENTER);
		lblDownloads.setText("Downloads");
		
		ProgressBar progressBar = new ProgressBar(compositeRightTopBottom, SWT.NONE);
		progressBar.setMinimum(0);
		progressBar.setMaximum(100);
		
		Composite compositeRightBottom = new Composite(sashFormTopRight, SWT.NONE);
		compositeRightBottom.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		TabFolder tabFolder = new TabFolder(compositeRightBottom, SWT.NONE);
		
		TabItem tbtmArchives = new TabItem(tabFolder, SWT.NONE);
		tbtmArchives.setText("Archives");
		
		Composite compositeTabArchive = new Composite(tabFolder, SWT.NONE);
		tbtmArchives.setControl(compositeTabArchive);
		compositeTabArchive.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		tableArchives = new Table(compositeTabArchive, SWT.BORDER | SWT.FULL_SELECTION);
		tableArchives.setHeaderVisible(true);
		tableArchives.setLinesVisible(true);
		
		TabItem tbtmDownloads = new TabItem(tabFolder, SWT.NONE);
		tbtmDownloads.setText("Downloads");
		
		Composite composite = new Composite(tabFolder, SWT.NONE);
		tbtmDownloads.setControl(composite);
		composite.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		tableDownloads = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
		tableDownloads.setHeaderVisible(true);
		tableDownloads.setLinesVisible(true);
		sashFormTopRight.setWeights(new int[] {1, 4});
		sashFormTop.setWeights(new int[] {1, 1});
		
		Composite compositeBottom = new Composite(sashForm, SWT.NONE);
		compositeBottom.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		ScrolledComposite scrolledComposite = new ScrolledComposite(compositeBottom, SWT.BORDER | SWT.V_SCROLL);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
		
		TextViewer textViewer = new TextViewer(scrolledComposite, SWT.BORDER);
		StyledText styledText = textViewer.getTextWidget();
		scrolledComposite.setContent(styledText);
		scrolledComposite.setMinSize(styledText.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		sashForm.setWeights(new int[] {4, 1});
		mntmAboutOwakeide.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				AboutDialog dialog = new AboutDialog(shlOwakeide.getShell(), SWT.DIALOG_TRIM);
				dialog.open();
			}
		});

		shlOwakeide.open();
		shlOwakeide.layout();
		
		FileInfoDialogTask.load();
		
		while (!shlOwakeide.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
}
